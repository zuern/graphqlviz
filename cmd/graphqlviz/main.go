// Copyright 2021 Kevin Zuern. All rights reserved.

package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"os"

	"gitlab.com/zuern/graphqlviz"
)

var (
	includeGGQLDefaultTypes = false
	format                  = "web"
)

func usage() {
	fmt.Fprintln(os.Stderr, `graphqlviz [options...] [file1...]

graphqlviz takes a list of GraphQL schema files and generates a directed graph
to visualize the objects in the schema. If no files are specified standard
input will be read.

options:`)
	flag.PrintDefaults()
	os.Exit(1)
}

func flags() {
	flag.BoolVar(&includeGGQLDefaultTypes, "defaultScalars", includeGGQLDefaultTypes,
		"Add default Int64 and Time scalars to schema before parsing.")
	flag.StringVar(&format, "f", format, "Format to output [graphviz-dot,mermaid,web]. If web is selected a browser will be opened to display the diagram.")
	flag.Usage = usage
	flag.Parse()
}

func main() {
	flags()
	sdl, err := loadSDL()
	if err != nil {
		exit(err)
	}
	g, err := graphqlviz.NewGraph(sdl, includeGGQLDefaultTypes)
	if err != nil {
		exit(err)
	}
	var out string
	switch format {
	case "graphviz-dot":
		out = g.DotNotation()
	case "mermaid":
		out = g.MermaidNotation()
	case "web":
		g.MermaidWeb()
	default:
		exit(fmt.Sprintf("invalid format specified: %q", format))
	}
	fmt.Println(out)
}

// loadSDL from files provided as program args or if none were provided read
// from stdin.
func loadSDL() (io.Reader, error) {
	if len(flag.Args()) == 0 {
		return os.Stdin, nil
	}
	var sdl bytes.Buffer
	for _, arg := range flag.Args() {
		f, err := os.Open(arg)
		if err != nil {
			return nil, err
		}
		defer func() { _ = f.Close() }()
		b, err := ioutil.ReadAll(f)
		if err != nil {
			return nil, err
		}
		_, _ = sdl.Write(b)
	}
	return &sdl, nil
}

func exit(err interface{}) {
	fmt.Fprintln(os.Stderr, err)
	os.Exit(1)
}
