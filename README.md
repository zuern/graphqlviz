GraphQLViz
-----------

An application that reads a graphql schema and generates an interactive
visualization of the relations between objects in the schema.

![](./assets/screenshot.jpg)

## Installation
```
cd cmd/graphqlviz && go install
```

## Usage

To visualize some schema files either pipe them to graphqlviz or pass as
arguments.

```
cat *.graphql | graphqlviz
graphqlviz query.graphql mutation.graphql
```

By default graphqlviz will open a browser to display the graph however using
`-f` text-based representations of the graph will be printed instead.

Optionally `-defaultScalars` can be used to add Time and Int64 scalars to the
schema before parsing.
