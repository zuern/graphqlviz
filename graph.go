// Copyright 2021 Kevin Zuern. All rights reserved.

package graphqlviz

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"os/exec"
	"os/signal"
	"runtime"
	"strings"
	"syscall"

	"github.com/uhn/ggql/pkg/ggql"
)

// Graph represents a parsed GraphQL schema and has methods to print
// different representations of it.
type Graph struct {
	// Nodes are the types in the Schema.
	Nodes map[string][]*Edge
}

// Edges are the relations between types in the schema.
type Edge struct {
	// Node that the edge is pointing to.
	Node string
	// Label is the label for the edge.
	Label string
}

// NewGraph returns a graph from the provided GraphQL schema. If
// includeDefaultScalars is true, default Int64 and Time scalars will be added
// to the schema.
func NewGraph(sdl io.Reader, includeDefaultScalars bool) (*Graph, error) {
	var root *ggql.Root
	if includeDefaultScalars {
		root = ggql.NewRoot(nil)
	} else {
		root = ggql.NewRoot(nil, "Time", "Int64")
	}
	if err := root.ParseReader(sdl); err != nil {
		return nil, err
	}
	g := &Graph{Nodes: make(map[string][]*Edge)}
	for _, t := range root.Types() {
		if t = base(t); t.Core() {
			continue
		}
		if tt, ok := t.(*ggql.Object); ok {
			g.addNode(tt.Name())
			for _, f := range tt.Fields() {
				ft, ok := base(f.Type).(*ggql.Object)
				if !ok {
					continue
				}
				g.addNode(ft.Name())
				g.addEdge(tt.N, ft.Name(), f.N)
			}
		}
	}
	return g, nil
}

// DotNotation returns a dot-language representation of the graph.
// https://graphviz.org/doc/info/lang.html
func (g *Graph) DotNotation() string {
	var b strings.Builder
	_, _ = b.WriteString(`digraph Schema {
	rank=LR;
	size="8,5"
	node [shape = circle];
`)
	for from := range g.Nodes {
		for _, edge := range g.Nodes[from] {
			_, _ = b.WriteString(fmt.Sprintf("\t%s -> %s\t[label = \"%s\" ];\n", from, edge.Node, edge.Label))
		}
	}
	_, _ = b.WriteString("}")
	return b.String()
}

// MermaidNotation returns a mermaid-language representation of the graph.
// https://mermaid-js.github.io/mermaid/#/
func (g *Graph) MermaidNotation() string {
	var b strings.Builder
	_, _ = b.WriteString("graph LR\n")
	nameMap := map[string]int{}
	var n int
	for node := range g.Nodes {
		nameMap[node] = n
		_, _ = b.WriteString(fmt.Sprintf("    node%d(%s)\n", n, node))
		n++
	}
	for from := range g.Nodes {
		for _, edge := range g.Nodes[from] {
			_, _ = b.WriteString(fmt.Sprintf("    node%d-->|%s|node%d\n", nameMap[from], edge.Label, nameMap[edge.Node]))
		}
	}
	return b.String()
}

// MermaidWeb generates a mermaid representation of the graph, starts a web
// server to serve the diagram, and opens the browser to the web page.
func (g *Graph) MermaidWeb() {
	diagram := g.MermaidNotation()
	page := fmt.Sprintf(`<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
</head>
<body>
  <div class="mermaid">
	%s
  </div>
 <script src="https://cdn.jsdelivr.net/npm/mermaid/dist/mermaid.min.js"></script>
 <script>mermaid.initialize({startOnLoad:true});</script>
</body>
</html>`, diagram)
	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, page)
	}))
	url := fmt.Sprintf("http://%s", svr.Listener.Addr())
	fmt.Printf("Serving diagram at %s\n", url)

	// Open the browser https://stackoverflow.com/a/39324149.
	var cmd string
	var args []string
	switch runtime.GOOS {
	case "windows":
		cmd = "cmd"
		args = []string{"/c", "start"}
	case "darwin":
		cmd = "open"
	default: // "linux", "freebsd", "openbsd", "netbsd"
		cmd = "xdg-open"
	}
	args = append(args, url)
	exec.Command(cmd, args...).Start()

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, syscall.SIGINT, syscall.SIGTERM)
	<-stop
	close(stop)
	svr.Close()
	fmt.Println("Shut down web server")
}

// Orphans returns the set of nodes in the graph that are not connected to any
// other node by an edge.
func (g *Graph) Orphans() []string {
	refs := make(map[string]int)
	for node, edges := range g.Nodes {
		if _, have := refs[node]; !have {
			refs[node] = 0
		}
		for _, e := range edges {
			if _, have := refs[e.Node]; !have {
				refs[e.Node] = 0
			}
			refs[e.Node]++
		}
	}
	var orphans []string
	for node, n := range refs {
		if n == 0 {
			orphans = append(orphans, node)
		}
	}
	return orphans
}

// addNode to graph if not already present.
func (g *Graph) addNode(name string) {
	if n := g.Nodes[name]; n == nil {
		g.Nodes[name] = []*Edge{}
	}
}

// addEdge from one node to another with the given label if not already
// present. Creates nodes as needed.
func (g *Graph) addEdge(from, to, label string) {
	g.addNode(from)
	g.addNode(to)
	var found bool
	for _, edge := range g.Nodes[from] {
		if edge.Node == to && edge.Label == label {
			found = true
			break
		}
	}
	if !found {
		g.Nodes[from] = append(g.Nodes[from], &Edge{Node: to, Label: label})
	}
}

// base returns the base type of t. For example if t is a [String!]!, String is
// returned.
func base(t ggql.Type) ggql.Type {
	switch tt := t.(type) {
	case *ggql.List:
		return base(tt.Base)
	case *ggql.NonNull:
		return base(tt.Base)
	default:
		return tt
	}
}
